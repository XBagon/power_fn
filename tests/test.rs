extern crate power_fn;
use power_fn::power_fn;

#[test]
fn test() {
    assert_eq!(alter_x(), 0);
    assert_eq!(alter_x_add1(), 1);
    assert_eq!(alter_x_stuff(), 1);
    assert_eq!(alter_x_add1_stuff(), 7);
}

#[power_fn]
fn alter_x() -> usize {
    let mut x = 0;

    #[power_fn(add1)]
    x += 1;

    #[power_fn(stuff)]
    {
        let y: usize = x * 2 + 5;
        x = y.pow(x as u32);
    }

    x
}
