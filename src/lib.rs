extern crate proc_macro;
use proc_macro::TokenStream;
use proc_macro2::Span;
use quote::quote;
use syn::{parse_macro_input, Attribute, Expr, Ident, Item, ItemFn, Stmt};

#[proc_macro_attribute]
pub fn power_fn(_attr: TokenStream, item: TokenStream) -> TokenStream {
    let fun = parse_macro_input!(item as ItemFn);
    let set = fn_power_set(fun);
    //panic!("{:#?}", fun)
    quote!(#(#set)*).into()
}

fn fn_power_set(mut fun: ItemFn) -> Vec<ItemFn> {
    let mut res = Vec::new();
    let mut index_name = None;
    for (i, mut stmt) in fun.block.stmts.iter_mut().enumerate() {
        let name = match &mut stmt {
            Stmt::Local(ref mut local) => clear_pfn_attribute(&mut local.attrs),
            Stmt::Item(ref mut item) => match item {
                Item::Const(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Item::Enum(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Item::ExternCrate(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Item::Fn(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Item::ForeignMod(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Item::Impl(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Item::Macro(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Item::Macro2(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Item::Mod(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Item::Static(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Item::Struct(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Item::Trait(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Item::TraitAlias(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Item::Type(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Item::Union(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Item::Use(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                _ => None,
            },
            Stmt::Expr(ref mut expr) => match expr {
                Expr::Array(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Assign(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::AssignOp(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Async(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Await(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Binary(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Block(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Box(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Break(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Call(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Cast(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Closure(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Continue(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Field(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::ForLoop(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Group(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::If(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Index(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Let(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Lit(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Loop(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Macro(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Match(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::MethodCall(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Paren(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Path(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Range(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Reference(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Repeat(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Return(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Struct(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Try(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::TryBlock(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Tuple(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Type(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Unary(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Unsafe(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::While(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Yield(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                _ => None,
            },
            Stmt::Semi(ref mut expr, _) => match expr {
                Expr::Array(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Assign(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::AssignOp(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Async(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Await(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Binary(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Block(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Box(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Break(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Call(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Cast(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Closure(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Continue(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Field(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::ForLoop(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Group(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::If(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Index(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Let(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Lit(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Loop(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Macro(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Match(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::MethodCall(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Paren(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Path(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Range(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Reference(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Repeat(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Return(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Struct(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Try(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::TryBlock(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Tuple(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Type(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Unary(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Unsafe(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::While(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                Expr::Yield(ref mut x) => clear_pfn_attribute(&mut x.attrs),
                _ => None,
            },
        };
        if let Some(name) = name {
            index_name = Some((i, name));
            break;
        }
    }
    if let Some((index, name)) = index_name {
        let mut fun_with = fun.clone();
        fun_with.sig.ident = Ident::new(&format!("{}_{}", &fun_with.sig.ident.to_string(), &name), Span::call_site());
        res.append(&mut fn_power_set(fun_with));
        fun.block.stmts.remove(index);
        res.append(&mut fn_power_set(fun));
    } else {
        res.push(fun);
    }

    res
}

fn clear_pfn_attribute(attrs: &mut Vec<Attribute>) -> Option<String> {
    let mut res = None;
    attrs.retain(|attr| {
        res = Some(attr.parse_args::<Ident>().unwrap().to_string());
        attr.path.get_ident().unwrap() != "power_fn"
    });
    res
}
